#include <filesystem>

#include <gtest/gtest.h>

#include <Invoker/Problem.h>
#include <Invoker/Submission.h>

#ifndef WORKING_DIRECTORY
#define WORKING_DIRECTORY ""
#endif

TEST(Problem, FromDirectory) {
  auto problem_directory = std::filesystem::path(WORKING_DIRECTORY) / "problems" / "0";
  auto problem = Problem::FromDirectory(problem_directory);
  ASSERT_EQ(problem->GetChecker(), problem_directory / "checker");
  ASSERT_EQ(problem->GetStatement(), problem_directory / "statement");
  ASSERT_EQ(problem->GetTests().size(), 1);
  for (const auto& test: problem->GetTests()) {
    ASSERT_EQ(test.GetInput(), problem_directory / "tests/1");
    ASSERT_EQ(test.GetAnswer(), problem_directory / "tests/1.a");
  }
}

TEST(Submission, CppSubmissionOk) {
  std::filesystem::path working_directory(WORKING_DIRECTORY);
  auto problem_directory = working_directory / "problems" / "0";
  auto submission_directory = working_directory / "submissions" / "0";

  auto problem = Problem::FromDirectory(problem_directory);

  auto submission = CppSubmission(problem,
                                  submission_directory,
                                  submission_directory / "main.cpp",
                                  "g++");

  ASSERT_EQ(submission.Run(), Verdict::OK);
}

TEST(Submission, CppSubmissionWa) {
  std::filesystem::path working_directory(WORKING_DIRECTORY);
  auto problem_directory = working_directory / "problems" / "0";
  auto submission_directory = working_directory / "submissions" / "1";

  auto problem = Problem::FromDirectory(problem_directory);

  auto submission = CppSubmission(problem,
                                  submission_directory,
                                  submission_directory / "main.cpp",
                                  "g++");

  ASSERT_EQ(submission.Run(), Verdict::WA);
}
