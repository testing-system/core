#include <fstream>
#include <iostream>
#include <vector>

// 0 - OK
// 1 - WA

// argv[1] - input
// argv[2] - ouput
// argv[3] - answer

template<typename T>
std::vector<T> ReadSeq(std::ifstream& input) {
  std::vector<T> result;
  T value;
  while (input >> value) {
    result.push_back(value);
  }

  return result;
}

int main(int argc, char* argv[]) {
  const int OK = 0;
  const int WA = 1;

  std::ifstream input(argv[1]);
  std::ifstream output(argv[2]);
  std::ifstream answer(argv[3]);

  auto output_seq = ReadSeq<int>(output);
  auto answer_seq = ReadSeq<int>(answer);
  if (output_seq.size() != answer_seq.size()) {
    return WA;
  }

  for (size_t i = 0; i < output_seq.size(); ++i) {
    if (output_seq[i] != answer_seq[i]) {
      return WA;
    }
  }

  return OK;
}