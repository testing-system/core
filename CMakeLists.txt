cmake_minimum_required(VERSION 3.16)
project(Core)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_FLAGS "-pthread -Wextra -Wall")

find_package(Boost REQUIRED COMPONENTS JSON)
find_package(cppzmq REQUIRED)
find_package(spdlog REQUIRED)

option(ENABLE_TESTS OFF)

include_directories(src)

add_subdirectory(src)

if (ENABLE_TESTS)
    find_package(GTest REQUIRED)
    add_subdirectory(test)
endif ()
