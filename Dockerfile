FROM ubuntu:20.04

RUN apt-get update

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
    build-essential \
    cmake \
    git \
    wget \
    libzmq3-dev

RUN git clone https://github.com/google/googletest.git && \
    cd googletest && \
    git checkout release-1.11.0 && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j $(nproc) && \
    make install

RUN wget https://boostorg.jfrog.io/artifactory/main/release/1.78.0/source/boost_1_78_0.tar.gz && \
    tar -xzf boost_1_78_0.tar.gz && \
    rm boost_1_78_0.tar.gz && \
    cd boost_1_78_0 && \
    ./bootstrap.sh && \
    ./b2 install

RUN git clone https://github.com/zeromq/cppzmq.git && \
    cd cppzmq && \
    git checkout v4.8.1 && \
    mkdir build && \
    cd build && \
    cmake -DCPPZMQ_BUILD_TESTS=OFF .. && \
    make -j $(nproc) && \
    make install

RUN git clone https://github.com/gabime/spdlog.git && \
    cd spdlog && \
    git checkout v1.9.2 && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j $(nproc) && \
    make install