#include <Invoker/Invoker.h>

int main(int argc, char* argv[]) {
  Config config;
  auto db_connector = std::make_shared<FakeDbConnector>();
  auto mq_connector = std::make_shared<ZmqConnector>("tcp://*:5555");

  Invoker invoker(config, db_connector, mq_connector);
  invoker.Run();
}
