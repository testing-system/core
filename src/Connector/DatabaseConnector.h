#pragma once

#include <unordered_map>

#include <Common/Types.h>
#include <Invoker/Submission.h>

class DatabaseConnector {
 public:
  virtual void SetVerdict(IdType submission_id, Verdict verdict) = 0;
};

class FakeDbConnector : public DatabaseConnector {
 public:
  void SetVerdict(IdType submission_id, Verdict verdict) override;

 private:
  std::unordered_map<IdType, Verdict> verdicts_;
};
