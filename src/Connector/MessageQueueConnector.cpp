#include <Connector/MessageQueueConnector.h>

ZmqConnector::ZmqConnector(const std::string& host) : context_(1), socket_(context_, zmq::socket_type::pull) {
  socket_.bind(host);
}

std::string ZmqConnector::Receive() {
  zmq::recv_result_t result;
  zmq::message_t request;

  do {
    result = socket_.recv(request);
  } while (!result.has_value());

  return request.to_string();
}
