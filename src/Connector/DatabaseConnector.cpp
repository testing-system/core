#include <spdlog/spdlog.h>

#include <Connector/DatabaseConnector.h>

std::string ToString(const Verdict& verdict) {
  switch (verdict) {
    case Verdict::OK: return "OK";
    case Verdict::CE: return "CE";
    case Verdict::RE: return "RE";
    case Verdict::CF: return "CF";
    case Verdict::WA: return "WA";
    default: return "Unknown";
  }
}

void FakeDbConnector::SetVerdict(IdType submission_id, Verdict verdict) {
  spdlog::info("[FakeDbConnector::SetVerdict] submission_id={}, verdict={}", submission_id, ToString(verdict));
  verdicts_[submission_id] = verdict;
}
