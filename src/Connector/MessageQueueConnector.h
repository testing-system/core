#pragma once

#include <memory>
#include <string>

#include <zmq.hpp>

class MessageQueueConnector {
 public:
  MessageQueueConnector() = default;
  MessageQueueConnector(const MessageQueueConnector& connector) = delete;
  MessageQueueConnector(MessageQueueConnector&& connector) = delete;

  MessageQueueConnector& operator=(const MessageQueueConnector& connector) = delete;
  MessageQueueConnector& operator=(MessageQueueConnector&& connector) = delete;

  virtual ~MessageQueueConnector() = default;

  virtual std::string Receive() = 0;
};

class ZmqConnector : public MessageQueueConnector {
 public:
  ZmqConnector(const std::string& host);
  ~ZmqConnector() override = default;

  std::string Receive() override;

 private:
  zmq::context_t context_;
  zmq::socket_t socket_;
};
