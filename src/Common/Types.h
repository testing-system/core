#pragma once

#include <filesystem>

using IdType = int64_t;
using PathType = std::filesystem::path;