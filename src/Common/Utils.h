#pragma once

#include <fstream>
#include <sstream>
#include <string>

#include <Common/Types.h>

std::string ReadFile(const PathType& path) {
  std::ifstream file(path);
  std::stringstream ss;
  ss << file.rdbuf();
  return ss.str();
}