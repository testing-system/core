#include <fcntl.h>
#include <sys/wait.h>
#include <unistd.h>

#include <boost/json.hpp>

#include <Invoker/Submission.h>

Submission::Submission(std::shared_ptr<Problem>&& problem,
                       PathType&& directory,
                       PathType&& solution)
    : problem_(std::move(problem)),
      directory_(std::move(directory)),
      solution_(std::move(solution)) {
}

const std::shared_ptr<Problem>& Submission::GetProblem() const {
  return problem_;
}

const PathType& Submission::GetDirectory() const {
  return directory_;
}

const PathType& Submission::GetSolution() const {
  return solution_;
}

CppSubmission::CppSubmission(std::shared_ptr<Problem> problem,
                             PathType directory,
                             PathType solution,
                             PathType compiler)
    : Submission(std::move(problem),
                 std::move(directory),
                 std::move(solution)),
      compiler_(std::move(compiler)),
      executable_(std::nullopt) {
}

Verdict CppSubmission::Run() {
  const PathType& directory = GetDirectory();

  auto pid = fork();

  if (pid == -1) {
    throw std::runtime_error("Fork failed");
  } else if (pid == 0) {
    execlp(compiler_.c_str(),
           "g++",
           GetSolution().c_str(),
           "-o",
           (directory / "executable").c_str(),
           nullptr);
  } else {
    int status;
    waitpid(pid, &status, 0);
    if (WIFEXITED(status) == 0 || WEXITSTATUS(status) != 0) {
      return Verdict::CE;
    }
    executable_ = directory / "executable";
  }

  auto output_file = directory / "output.txt";
  for (const auto& test : GetProblem()->GetTests()) {
    pid = fork();
    if (pid == -1) {
      throw std::runtime_error("Fork failed");
    } else if (pid == 0) {
      int in_fd = open(test.GetInput().c_str(), O_RDONLY);
      int out_fd = open(output_file.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0666);
      dup2(in_fd, STDIN_FILENO);
      dup2(out_fd, STDOUT_FILENO);
      execlp(executable_->c_str(), "executable", nullptr);
    } else {
      int status;
      waitpid(pid, &status, 0);
      if (WIFEXITED(status) == 0 || WEXITSTATUS(status) != 0) {
        return Verdict::RE;
      }
    }

    pid = fork();
    if (pid == -1) {
      throw std::runtime_error("Fork failed");
    } else if (pid == 0) {
      execlp(GetProblem()->GetChecker().c_str(),
             "checker",
             test.GetInput().c_str(),
             output_file.c_str(),
             test.GetAnswer().c_str(),
             nullptr);
    } else {
      int status;
      waitpid(pid, &status, 0);
      if (WIFEXITED(status) == 0) {
        return Verdict::CF;
      }
      if (WEXITSTATUS(status) == 1) {
        return Verdict::WA;
      }
    }
  }

  return Verdict::OK;
}
