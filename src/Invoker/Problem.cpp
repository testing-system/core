#include <boost/json.hpp>

#include <Common/Utils.h>
#include <Invoker/Problem.h>

Problem::Test::Test(PathType input, PathType answer) : input_(std::move(input)), answer_(std::move(answer)) {
}

const PathType& Problem::Test::GetInput() const {
  return input_;
}

const PathType& Problem::Test::GetAnswer() const {
  return answer_;
}

Problem::Problem(PathType checker,
                 PathType statement,
                 std::vector<Test> tests)
    : checker_(std::move(checker)),
      statement_(std::move(statement)),
      tests_(std::move(tests)) {
}

const PathType& Problem::GetChecker() const {
  return checker_;
}

const PathType& Problem::GetStatement() const {
  return statement_;
}

const std::vector<Problem::Test>& Problem::GetTests() const {
  return tests_;
}

std::shared_ptr<Problem> Problem::FromDirectory(const PathType& directory) {
  auto json_object = boost::json::parse(ReadFile(directory / "config.json")).as_object();
  PathType checker = directory / json_object["checker"].as_string().c_str();
  PathType statement = directory / json_object["statement"].as_string().c_str();
  std::vector<Test> tests;
  for (const auto& test : json_object["tests"].as_array()) {
    const auto& test_object = test.as_object();
    tests.emplace_back(directory / test_object.at("input").as_string().c_str(),
                       directory / test_object.at("answer").as_string().c_str());
  }

  return std::make_shared<Problem>(std::move(checker), std::move(statement), std::move(tests));
}
