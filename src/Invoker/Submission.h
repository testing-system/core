#pragma once

#include <filesystem>
#include <memory>
#include <optional>

#include <Common/Types.h>
#include <Invoker/Problem.h>

enum class Verdict {
  OK,
  CE,
  RE,
  CF,
  WA,
};

class Submission {
 public:
  Submission(std::shared_ptr<Problem>&& problem,
             PathType&& directory,
             PathType&& solution);

  Submission(const Submission& submission) = default;
  Submission(Submission&& submission) = default;

  Submission& operator=(const Submission& submission) = delete;
  Submission& operator=(Submission&& submission) = delete;

  virtual ~Submission() = default;

  const std::shared_ptr<Problem>& GetProblem() const;
  const PathType& GetDirectory() const;
  const PathType& GetSolution() const;

  virtual Verdict Run() = 0;

 private:
  std::shared_ptr<Problem> problem_;
  PathType directory_;
  PathType solution_;
};

class CppSubmission : public Submission {
 public:
  CppSubmission(std::shared_ptr<Problem> problem,
                PathType directory,
                PathType solution,
                PathType compiler);

  Verdict Run() override;

 private:
  PathType compiler_;
  std::optional<PathType> executable_;
};
