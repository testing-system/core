#pragma once

#include <unordered_map>

#include <Connector/DatabaseConnector.h>
#include <Connector/MessageQueueConnector.h>
#include <Invoker/Config.h>
#include <Invoker/Problem.h>

class Invoker {
 public:
  explicit Invoker(Config config,
                   std::shared_ptr<DatabaseConnector> db_connector,
                   std::shared_ptr<MessageQueueConnector> mq_connector);

  Invoker(const Invoker& invoker) = delete;
  Invoker(Invoker&& invoker) = delete;

  Invoker& operator=(const Invoker& invoker) = delete;
  Invoker& operator=(Invoker&& invoker) = delete;

  ~Invoker() = default;

  void Run();

 private:
  void ProcessMessage(const std::string& message);

  bool stop_ = false;
  Config config_;
  std::shared_ptr<DatabaseConnector> db_connector_;
  std::shared_ptr<MessageQueueConnector> mq_connector_;
  std::unordered_map<IdType, std::shared_ptr<Problem>> problems_;
};
