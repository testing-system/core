#pragma once

#include <filesystem>
#include <vector>

#include <Common/Types.h>

class Problem {
 public:
  class Test {
   public:
    Test(PathType input, PathType answer);

    Test(const Test& test) = default;
    Test(Test&& test) = default;

    Test& operator=(const Test& test) = default;
    Test& operator=(Test&& test) = default;

    ~Test() = default;

    const PathType& GetInput() const;
    const PathType& GetAnswer() const;

   private:
    PathType input_;
    PathType answer_;
  };

  Problem(PathType checker,
          PathType statement,
          std::vector<Test> tests);

  Problem(const Problem& problem) = default;
  Problem(Problem&& problem) = default;

  Problem& operator=(const Problem& problem) = default;
  Problem& operator=(Problem&& problem) = default;

  ~Problem() = default;

  const PathType& GetChecker() const;
  const PathType& GetStatement() const;
  const std::vector<Test>& GetTests() const;

  static std::shared_ptr<Problem> FromDirectory(const PathType& directory);

 private:
  PathType checker_;
  PathType statement_;
  std::vector<Test> tests_;
};
