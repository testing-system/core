#pragma once

#include <string>

class Config {
 public:
  Config() = default;

  Config(const Config& config) = default;
  Config(Config&& config) = default;

  Config& operator=(const Config& config) = default;
  Config& operator=(Config&& config) = default;

  ~Config() = default;
};
