#include <boost/json.hpp>
#include <spdlog/spdlog.h>

#include <Invoker/Invoker.h>
#include <Invoker/Submission.h>

Invoker::Invoker(Config config,
                 std::shared_ptr<DatabaseConnector> db_connector,
                 std::shared_ptr<MessageQueueConnector> mq_connector)
    : config_(std::move(config)), db_connector_(std::move(db_connector)), mq_connector_(std::move(mq_connector)) {
}

void Invoker::Run() {
  spdlog::info("[Invoker::Run] Starting invoker");
  boost::system::error_code error_code;
  while (!stop_) {
    spdlog::debug("[Invoker::Run] Waiting for message");
    ProcessMessage(mq_connector_->Receive());
  }
}

void LogMissingField(std::string_view key, const std::string& message) {
  spdlog::error("[Invoker::ProcessMessage] Missing required field \"{}\" while parsing \"{}\"", key, message);
}

void LogUnexpectedType(std::string_view key, std::string_view expected_type, const std::string& message) {
  spdlog::error("[Invoker::ProcessMessage] Unexpected type of field \"{}\" (expected \"{}\") while parsing \"{}\"",
                key, expected_type, message);
}

void LogParsedMessageType(std::string_view type) {
  spdlog::info("[Invoker::ProcessMessage] Parsed message as \"{}\"", type);
}

void Invoker::ProcessMessage(const std::string& message) {
  static constexpr std::string_view kExitMessageType = "exit_message";
  static constexpr std::string_view kAddProblemMessageType = "add_problem";
  static constexpr std::string_view kProcessSubmissionMessageType = "process_submission";

  spdlog::info("[Invoker::ProcessMessage] Processing message: \"{}\"", message);
  boost::system::error_code error_code;
  auto value = boost::json::parse(message, error_code);
  if (error_code) {
    spdlog::error("[Invoker::ProcessMessage] Can't parse \"{}\": \"{}\"", message, error_code.message());
    return;
  }

  const auto& object = value.as_object();
  auto type_ptr = object.if_contains("type");
  if (type_ptr == nullptr) {
    LogMissingField("type", message);
    return;
  } else if (!type_ptr->is_string()) {
    LogUnexpectedType("type", "string", message);
  }
  auto& type = type_ptr->as_string();

  if (type == kExitMessageType) {
    LogParsedMessageType(kExitMessageType);
    stop_ = true;
  } else if (type == kAddProblemMessageType) {
    LogParsedMessageType(kAddProblemMessageType);
    auto id_ptr = object.if_contains("id");
    if (id_ptr == nullptr) {
      LogMissingField("id", message);
      return;
    } else if (!id_ptr->is_int64()) {
      LogUnexpectedType("id", "int64", message);
    }

    auto directory_ptr = object.if_contains("directory");
    if (directory_ptr == nullptr) {
      LogMissingField("directory", message);
      return;
    } else if (!directory_ptr->is_string()) {
      LogUnexpectedType("directory", "string", message);
    }

    auto id = id_ptr->as_int64();
    auto& directory = directory_ptr->as_string();

    spdlog::debug("[Invoker::ProcessMessage] id=\"{}\", directory=\"{}\"", id, directory);

    if (problems_.find(id) != problems_.end()) {
      spdlog::warn("[Invoker::ProcessMessage] Adding existing problem \"{}\"", id);
    }

    problems_[id] = Problem::FromDirectory(directory.c_str());
    spdlog::info("[Invoker::ProcessMessage] Added problem \"{}\"", id);
  } else if (type == kProcessSubmissionMessageType) {
    LogParsedMessageType(kProcessSubmissionMessageType);

    auto problem_id_ptr = object.if_contains("problem_id");
    if (problem_id_ptr == nullptr) {
      LogMissingField("problem_id", message);
      return;
    } else if (!problem_id_ptr->is_int64()) {
      LogUnexpectedType("problem_id", "int64", message);
    }

    auto submission_id_ptr = object.if_contains("submission_id");
    if (submission_id_ptr == nullptr) {
      LogMissingField("submission_id", message);
      return;
    } else if (!submission_id_ptr->is_int64()) {
      LogUnexpectedType("submission_id", "int64", message);
    }

    auto directory_ptr = object.if_contains("directory");
    if (directory_ptr == nullptr) {
      LogMissingField("directory", message);
      return;
    } else if (!directory_ptr->is_string()) {
      LogUnexpectedType("directory", "string", message);
    }

    auto solution_ptr = object.if_contains("solution");
    if (solution_ptr == nullptr) {
      LogMissingField("solution", message);
      return;
    } else if (!solution_ptr->is_string()) {
      LogUnexpectedType("solution", "string", message);
    }

    auto compiler_ptr = object.if_contains("compiler");
    if (compiler_ptr == nullptr) {
      LogMissingField("compiler", message);
      return;
    } else if (!compiler_ptr->is_string()) {
      LogUnexpectedType("compiler", "string", message);
    }

    auto problem_id = problem_id_ptr->as_int64();
    auto problem_iterator = problems_.find(problem_id);
    if (problem_iterator == problems_.end()) {
      spdlog::error("[Invoker::ProcessMessage] Invalid problem_id \"{}\" in message \"{}\"", problem_id, message);
      return;
    }
    auto submission_id = submission_id_ptr->as_int64();
    auto& directory = directory_ptr->as_string();
    auto& solution = solution_ptr->as_string();
    auto& compiler = compiler_ptr->as_string();

    spdlog::debug("[Invoker::ProcessMessage] problem_id=\"{}\", submission_id=\"{}\", directory=\"{}\", "
                  "solution=\"{}\", compiler=\"{}\"", problem_id, submission_id, directory, solution, compiler);

    CppSubmission submission(problem_iterator->second, directory.c_str(), solution.c_str(), compiler.c_str());
    db_connector_->SetVerdict(submission_id, submission.Run());
  } else {
    spdlog::error("[Invoker::ProcessMessage] Incorrect type \"{}\" of message \"{}\"", type, message);
  }
}
